const express = require('express');
const router = express.Router();

const sController = require('../Controllers/CE611998005Controller');
const validator =require('../controllers/CE611998005Validator');

router.get('/CE611998005',sController.list);
router.get('/CE611998005/add',sController.add);
router.post('/CE611998005/save',validator.add,sController.save);

router.get('/CE611998005/del/:id',sController.del);
router.get('/CE611998005/delete/:id',sController.delete);

router.get('/CE611998005/edit/:id',sController.edit);
router.post('/CE611998005/update/:id',validator.update,sController.update );


module.exports = router;
