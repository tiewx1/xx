const express = require('express');
const router = express.Router();

const sController = require('../Controllers/CE611998019Controller');
const validator =require('../controllers/CE611998019Validator');

router.get('/CE611998019',sController.list);
router.get('/CE611998019/add',sController.add);
router.post('/CE611998019/save',validator.add,sController.save);

router.get('/CE611998019/del/:id',sController.del);
router.get('/CE611998019/delete/:id',sController.delete);

router.get('/CE611998019/edit/:id',sController.edit);
router.post('/CE611998019/update/:id',validator.update,sController.update );


module.exports = router;
