

const express=require('express');
const router=express.Router();

const ce6023 =require("../controllers/CE601998023Controller");
const validator = require('../controllers/CE601998023Validator');
router.get('/CE601998023',ce6023.list);
router.post('/CE601998023/save',validator.addValidator,ce6023.save);
router.get('/CE601998023/del/:id',ce6023.del);
router.get('/CE601998023/delete/:id',ce6023.delete);
router.get('/CE601998023/update/:id',validator.editValidator,ce6023.edit);
router.post('/CE601998023/update/:id',validator.addValidator,ce6023.update);
router.get('/CE601998023/add',ce6023.add);

module.exports=router;
