const express = require('express');
const router = express.Router();

const homeController = require('../controllers/F3K1Controller');
const validator = require('../controllers/F3K1Validator');

router.get('/', homeController.log);
router.post('/login', validator.login, homeController.login);
router.get('/home', homeController.home);
router.get('/logout', homeController.logout);

router.get('/user', homeController.list);
router.get('/user/add', homeController.add);
router.post('/user/save', validator.login, homeController.save);

router.get('/user/del/:id', homeController.del);
router.get('/user/delete/:id', homeController.delete);

router.get('/user/edit/:id', homeController.edit);
router.post('/user/update/:id', validator.login, homeController.update);

module.exports = router;