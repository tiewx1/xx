const express = require('express');
const router = express.Router();

const sController = require('../Controllers/CE611998002Controller');
const validator =require('../controllers/CE611998002Validator');

router.get('/CE611998002',sController.list);
router.get('/CE611998002/add',sController.add);
router.post('/CE611998002/save',validator.add,sController.save);

router.get('/CE611998002/del/:id',sController.del);
router.get('/CE611998002/delete/:id',sController.delete);

router.get('/CE611998002/edit/:id',sController.edit);
router.post('/CE611998002/update/:id',validator.update,sController.update );


module.exports = router;
