const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');
const session = require('express-session');
const mysql = require('mysql');
const connection = require('express-myconnection')
const app = express();

app.use(express.static('public'));
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(body.urlencoded({ extended: true }));
app.use(cookie());
app.use(session({
    secret: 'Passw0rd',
    resave: true,
    saveUninitialized: true
}));

app.use(connection(mysql, {
    host: 'localhost',
    user: 'root',
    password: 'Alohomora',
    port: 3306,
    database: 'f3k1'
}, 'single'));



const routexx = require('./routes/F3K1Route');
app.use('/', routexx);


const routeploy = require('./routes/CE601998023Route');
app.use('/', routeploy);


const routebas = require('./routes/CE611998002Route');
app.use('/', routebas);

const routeprem = require('./routes/CE611998005Route');
app.use('/', routeprem);


const routetiew = require('./routes/CE611998019Route');
app.use('/', routetiew);


app.listen('8081');
