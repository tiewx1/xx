const controller = {};
const e = require('express');
const { validationResult } = require('express-validator');

controller.list = function(req, res) {
    if (req.session.user) {
        req.getConnection((err, conn) => {
            conn.query('select tiew.ID as tiewID,tiew.SCIT611998019 as tiewSCIT,tiew.MIROT611998019 as tiewMIROT,tiew.ID_ploy as tiewID_ploy,tiew.ID_prem as tiewID_prem,ploy.ID as ployID,ploy.SCIT601998023 as ploySCIT,ploy.MIROT601998023 as ployMIROT,prem.ID as premID,prem.SCIT611998005 as premSCIT,prem.MIROT611998005 as premMIROT from ce611998019 as tiew left JOIN ce601998023 as ploy  ON tiew.ID_ploy = ploy.ID left JOIN ce611998005 as prem ON tiew.ID_prem = prem.ID ;',
                function(err, xx) {
                    if (err) {
                        res.json(err);
                    } else {
                        res.render('CE611998019/CE611998019List', {
                            data: xx,
                            session: req.session
                        });
                    }
                });
        });
    } else {
        res.redirect('/');
    };
};

controller.add = (req, res) => {
    if (req.session.user) {
        if (req.session.modulece611998019 == 1) {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM ce601998023;', function(err, ploy) {
                    conn.query('SELECT * FROM ce611998005;', function(err, prem) {
                        res.render('CE611998019/CE611998019Add', {
                            data2: ploy,
                            data3: prem,
                            session: req.session
                        });
                    });
                });
            });
        } else {
            res.redirect('/ce611998019');
        };
    };
};

controller.save = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998019 === 1) {
            const data = req.body
            console.log(data);;
            if (data.ID_ploy == "") { data.ID_ploy = null; }
            if (data.ID_prem == "") { data.ID_prem = null; }
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                res.redirect('/CE611998019/add');
            } else {
                req.session.success = true;
                req.session.topic = "เพิ่มข้อมูลสำเร็จ";
                req.getConnection(function(err, conn) {
                    conn.query('insert into ce611998019 set ?', [data], function(err, xx) {
                        res.redirect('/CE611998019');
                    });
                });
            };
        } else {
            res.redirect('/ce611998019');
        };
    };
};

controller.del = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998019 === 1) {
            const { id } = req.params;
            req.getConnection(function(err, conn) {
                conn.query('select tiew.ID as tiewID,tiew.SCIT611998019 as tiewSCIT,tiew.MIROT611998019 as tiewMIROT,tiew.ID_ploy as tiewID_ploy,tiew.ID_prem as tiewID_prem,ploy.ID as ployID,ploy.SCIT601998023 as ploySCIT,ploy.MIROT601998023 as ployMIROT,prem.ID as premID,prem.SCIT611998005 as premSCIT,prem.MIROT611998005 as premMIROT from ce611998019 as tiew left JOIN ce601998023 as ploy  ON tiew.ID_ploy = ploy.ID left JOIN ce611998005 as prem ON tiew.ID_prem = prem.ID HAVING tiewID = ?;', [id], function(err, xx) {
                    if (err) {
                        const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลได้', param: '', location: '' }] }
                        req.session.errors = errorss;
                        req.session.success = false;
                    }
                    res.render('CE611998019/CE611998019Delete', {
                        data: xx[0],
                        session: req.session
                    });
                });
            });
        } else {
            res.redirect('/ce611998019');
        };
    };
};

controller.delete = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998019 === 1) {
            const { id } = req.params;

            console.log(req.params);
            req.getConnection(function(err, conn) {
                conn.query('DELETE FROM ce611998019 WHERE ID = ?', [id], function(err2, xx) {
                    if (err2) {
                        const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
                        req.session.errors = errorss;
                        req.session.success = false;
                        res.redirect('/CE611998019');
                    }else {
                      console.log(err2);
                      req.session.success = true;
                      req.session.topic = "ลบสำเร็จ";
                      res.redirect('/CE611998019');
                    }
                });
            });
        } else {
            res.redirect('/ce611998019');
        };
    };
};

controller.edit = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998019 === 1) {
            const { id } = req.params;
            req.getConnection(function(err, conn) {
                conn.query('SELECT * FROM ce611998019  WHERE ID = ?', [id], function(err, tiew) {
                    conn.query('SELECT * FROM ce601998023', [id], function(err, ploy) {
                        conn.query('SELECT * FROM ce611998005', [id], function(err, prem) {
                            if (err) {
                                res.json(err);
                            }
                            res.render('CE611998019/CE611998019Update', {
                                data1: tiew[0],
                                data2: ploy,
                                data3: prem,
                                session: req.session
                            });
                        });
                    });
                });
            });
        } else {
            res.redirect('/ce611998019');
        };
    };
};

controller.update = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998019 === 1) {
            const { id } = req.params;
            const data = req.body;
            if (data.ID_ploy == "") { data.ID_ploy = null; }
            if (data.ID_prem == "") { data.ID_prem = null; }
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                req.getConnection(function(err, conn) {
                    conn.query('SELECT * FROM ce611998019 WHERE ID = ?', [id], function(err, tiew) {
                        conn.query('SELECT * FROM ce601998023', [id], function(err, ploy) {
                            conn.query('SELECT * FROM ce611998005', [id], function(err, prem) {
                                if (err) {
                                    res.json(err);
                                }
                                res.render('CE611998019/CE611998019Update', {
                                    data1: tiew[0],
                                    data2: ploy,
                                    data3: prem,
                                    session: req.session
                                });
                            });
                        });
                    });
                });
            } else {
                req.session.success = true;
                req.session.topic = "แก้ไขเรียบร้อย";
                req.getConnection(function(err, conn) {
                    conn.query('update ce611998019 set ? where ID = ?', [data, id], (err, tiew) => {
                        if (err) {
                            console.log(err);
                            (err);
                        }
                        res.redirect('/CE611998019')
                    });
                });
            };
        } else {
            res.redirect('/ce611998019');
        };
    };
};


module.exports = controller;
