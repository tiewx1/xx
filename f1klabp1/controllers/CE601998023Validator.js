
const { check } = require('express-validator');

exports.addValidator = [check('SCIT601998023',"SCIT_ไม่ถูกต้อง").not().isEmpty(),
                        check('MIROT601998023',"MIROT_ไม่ถูกต้อง").isFloat()];

exports.editValidator = [check('SCIT601998023',"SCIT_ไม่ถูกต้อง").not().isEmpty(),
                        check('MIROT601998023',"MIROT_ไม่ถูกต้อง").isFloat()];
