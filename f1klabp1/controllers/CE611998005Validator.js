const { check } = require('express-validator');//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator

exports.add = [check('SCIT611998005',"SCIT611998005 -ไม่ถูกต้อง").not().isEmpty(),
               check('MIROT611998005',"MIROT611998005 -ไม่ถูกต้อง").isFloat()
              ];

exports.update = [check('SCIT611998005',"SCIT611998005 -ไม่ถูกต้อง").not().isEmpty(),
                  check('MIROT611998005',"MIROT611998005 -ไม่ถูกต้อง").isFloat()
                 ];
