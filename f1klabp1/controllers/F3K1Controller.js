const controller = {};
const { validationResult } = require('express-validator');
const { default: validator } = require('validator');

controller.log = function(req, res) {
    res.render('login', { session: req.session });
};
controller.home = function(req, res) {
    if (req.session.user) {
        res.render('index', { session: req.session });
    } else {
        res.redirect('/');
    };
};
controller.login = function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        req.session.errors = errors;
        req.session.success = false;
        res.redirect('/');
    } else {
        req.getConnection(function(err, conn) {
            conn.query('SELECT * FROM login WHERE username = ? AND password = ?', [req.body.username, req.body.password], (err, data) => {
                if (err) {
                    res.json(err);
                } else {
                    if (data.length > 0) {
                        req.session.user = data[0].id;
                        conn.query('select * from permission where id_module = 1 and id_login = ?', [req.session.user], function(err2, module1) {
                            if (err2) {
                                res.json(err2);
                            } else {
                                if (module1.length > 0) {
                                    req.session.module1 = 1;
                                    req.session.modulece601998023 = 1;
                                    req.session.modulece611998005 = 1;
                                    req.session.modulece611998002 = 1;
                                    req.session.modulece611998019 = 1;
                                    console.log(req.session.module);
                                    res.render('index', { session: req.session });
                                } else {
                                    conn.query('select * from permission where id_module = 2 and id_login = ?', [req.session.user], function(err3, modulece601998023) {
                                        if (err3) {
                                            res.json(err3);
                                        } else {
                                            if (modulece601998023.length > 0) {
                                                req.session.modulece601998023 = 1;
                                                console.log(req.session.module);
                                                res.render('index', { session: req.session });
                                            } else {
                                                conn.query('select * from permission where id_module = 3 and id_login = ?', [req.session.user], function(err4, modulece611998005) {
                                                    if (err4) {
                                                        res.json(err4);
                                                    } else {
                                                        if (modulece611998005.length > 0) {
                                                            req.session.modulece611998005 = 1;
                                                            console.log(req.session.module);
                                                            res.render('index', { session: req.session });
                                                        } else {
                                                            conn.query('select * from permission where id_module = 4 and id_login = ?', [req.session.user], function(err5, modulece611998002) {
                                                                if (err5) {
                                                                    res.json(err5);
                                                                } else {
                                                                    if (modulece611998002.length > 0) {
                                                                        req.session.modulece611998002 = 1;
                                                                        console.log(req.session.module);
                                                                        res.render('index', { session: req.session });
                                                                    } else {
                                                                        conn.query('select * from permission where id_module = 5 and id_login = ?', [req.session.user], function(err6, modulece611998019) {
                                                                            if (err6) {
                                                                                res.json(err6);
                                                                            } else {
                                                                                if (modulece611998019.length > 0) {
                                                                                    req.session.modulece611998019 = 1;
                                                                                    console.log(req.session.module);
                                                                                    res.render('index', { session: req.session });
                                                                                } else {
                                                                                    req.session.moduleceuser = 0;
                                                                                    console.log(req.session.module);
                                                                                    res.render('index', { session: req.session });
                                                                                }
                                                                            };
                                                                        });
                                                                    };
                                                                };
                                                            });
                                                        };
                                                    };
                                                });
                                            };
                                        };
                                    });
                                };
                            };
                        });

                    } else {
                        req.session.errors = errors;
                        req.session.success = false;
                        res.redirect('/');
                    };
                };
            });
        });
    };
};

controller.logout = function(req, res) {
    req.session.destroy();
    res.redirect('/');
};

/////////////////////////////////////////////////////////////////////////////////////
controller.list = function(req, res) {
    if (req.session.user) {
        req.getConnection((err, conn) => {
            conn.query('SELECT * from login', function(err, login) {
                if (err) {
                    res.json(err);
                } else {
                    res.render('AddLogin/loginList', {
                        data: login,
                        session: req.session
                    });
                };
            });
        });
    } else {
        res.redirect('/');
    };
};

controller.add = function(req, res) {
    if (req.session.user) {
        if (req.session.module1 == 1) {
            req.getConnection(function(err, conn) {
                conn.query('SELECT * FROM module', function(err, module) {
                    conn.query('SELECT * FROM permission', function(err, permission) {
                        res.render('AddLogin/loginAdd', {
                            data1: module,
                            data2: permission,
                            session: req.session
                        });
                    });
                });

            });
        } else {
            res.redirect('/user');
        };
    };
};


controller.save = function(req, res) {
    if (req.session.user) {
        if (req.session.module1 == 1) {
            const data = req.body;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                res.redirect('/user/add');
            } else {
                req.session.success = true;
                req.session.topic = "เพิ่มข้อมูลสำเร็จ";
                req.getConnection(function(err, conn) {
                    console.log(data);
                    conn.query('insert into login set ?', [data], function(err, bas) {
                        if (err) {
                            console.log(err);
                        }
                        res.redirect('/user');
                    });
                });
            };
        } else {
            res.redirect('/user');
        };
    };
};

controller.del = function(req, res) {
    if (req.session.user) {
        if (req.session.module1 == 1) {
            const { id } = req.params;
            req.getConnection(function(err, conn) {
                conn.query('SELECT * from login where id = ?', [id], function(err, login) {
                    if (err) {
                        const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลได้', param: '', location: '' }] }
                        req.session.errors = errorss;
                        req.session.success = false;
                    }
                    console.log(login);
                    res.render('AddLogin/loginDelete', {
                        data: login[0],
                        session: req.session
                    });
                });
            });
        } else {
            res.redirect('/user');
        };
    };
};

controller.delete = function(req, res) {
    if (req.session.user) {
        if (req.session.module1 == 1) {
            const { id } = req.params;
            console.log(req.params);
            req.getConnection(function(err, conn) {
                conn.query('DELETE FROM login WHERE id = ?', [id], function(err, login) {
                    if (err) {
                        const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
                        req.session.errors = errorss;
                        req.session.success = false;
                    }
                    console.log(login);
                    req.session.success = true;
                    req.session.topic = "ลบสำเร็จ";
                    res.redirect('/user');
                });
            });
        } else {
            res.redirect('/user');
        };
    };
};

controller.edit = function(req, res) {
    if (req.session.user) {
        if (req.session.module1 == 1) {
            const { id } = req.params;
            req.getConnection(function(err, conn) {
                conn.query('SELECT * FROM  login WHERE ID = ?;', [id], function(err, login) {
                    if (err) {
                        res.json(err);
                    }
                    res.render('AddLogin/loginUpdate', {
                        data: login[0],
                        session: req.session
                    });
                });
            });
        } else {
            res.redirect('/user');
        };
    };
};

controller.update = function(req, res) {
    if (req.session.user) {
        if (req.session.module1 == 1) {
            const { id } = req.params;
            const data = req.body;
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                req.getConnection(function(err, conn) {
                    conn.query('UPDATE login SET ? WHERE ID = ?', [id], function(err, bas) {
                        if (err) {
                            res.json(err);
                        }
                        res.render('AddLogin/loginUpdate', {
                            data: bas[0],

                            session: req.session
                        });
                    });
                });
            } else {
                req.session.success = true;
                req.session.topic = "แก้ไขเรียบร้อย";
                req.getConnection(function(err, conn) {
                    conn.query('update login set ? where id = ?', [data, id], (err, bas) => {
                        if (err) {
                            res.json(err);
                        }
                        res.redirect('/user')
                    });
                });
            };
        } else {
            res.redirect('/user');
        };
    };
};
module.exports = controller;