const { check } = require('express-validator');//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator

exports.add = [check('SCIT611998002',"SCIT611998002 -ไม่ถูกต้อง").not().isEmpty(),
               check('MIROT611998002',"MIROT611998002 -ไม่ถูกต้อง").isFloat()
              ];

exports.update = [check('SCIT611998002',"SCIT611998002 -ไม่ถูกต้อง").not().isEmpty(),
                  check('MIROT611998002',"MIROT611998002 -ไม่ถูกต้อง").isFloat()
                 ];
