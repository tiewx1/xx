const controller = {};
const e = require('express');
const { validationResult } = require('express-validator');

controller.list = function(req, res) {
    if (req.session.user) {
        req.getConnection((err, conn) => {
            conn.query('SELECT bas.ID as bID, bas.SCIT611998002 as bSCIT611998002, bas.MIROT611998002 as bMIROT611998002, bas.ID_tiew as bID_tiew, bas.ID_ploy as bID_ploy, t.SCIT611998019 as tSCIT611998019 , p.SCIT601998023 as pSCIT601998023 FROM CE611998002 as bas  left join CE611998019 as t on bas.id_tiew = t.id left join CE601998023 as p on bas.id_ploy = p.id',
                function(err, bas) {
                    if (err) {
                        res.json(err);
                    } else {
                        res.render('CE611998002/CE611998002List', {
                            data: bas,
                            session: req.session
                        });
                    }
                });
        });
    } else {
        res.redirect('/');
    };
};

controller.add = (req, res) => {
    if (req.session.user) {
        if (req.session.modulece611998002 == 1) {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM ce611998019;', function(err, tiew) {
                    conn.query('SELECT * FROM ce601998023;', function(err, ploy) {
                        res.render('CE611998002/CE611998002Add', {
                            data2: tiew,
                            data3: ploy,
                            session: req.session
                        });
                    });
                });
            });
        } else {
            res.redirect('/ce611998002');
        };
    };
};

controller.save = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998002 === 1) {
            const data = req.body;
            if (data.ID_tiew == "") { data.ID_tiew = null; }
            if (data.ID_ploy == "") { data.ID_ploy = null; }
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                res.redirect('/CE611998002/add');
            } else {
                req.session.success = true;
                req.session.topic = "เพิ่มข้อมูลสำเร็จ";
                req.getConnection(function(err, conn) {
                    conn.query('insert into ce611998002 set ?', [data], function(err, bas) {
                        res.redirect('/CE611998002');
                    });
                });
            };
        } else {
            res.redirect('/ce611998002');
        };
    };
};

controller.del = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998002 === 1) {
            const { id } = req.params;
            req.getConnection(function(err, conn) {
                conn.query('SELECT bas.ID as bID, bas.SCIT611998002 as bSCIT611998002, bas.MIROT611998002 as bMIROT611998002, bas.ID_tiew as bID_tiew, bas.ID_ploy as bID_ploy, t.SCIT611998019 as tSCIT611998019 , p.SCIT601998023 as pSCIT601998023 FROM CE611998002 as bas  left join CE611998019 as t on bas.id_tiew = t.id left join CE601998023 as p on bas.id_ploy = p.id HAVING bID = ?', [id], function(err, bas) {
                    if (err) {
                        const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลได้', param: '', location: '' }] }
                        req.session.errors = errorss;
                        req.session.success = false;
                    }
                    console.log(bas);
                    res.render('CE611998002/CE611998002Delete', {
                        data: bas[0],
                        session: req.session
                    });
                });
            });
        } else {
            res.redirect('/ce611998002');
        };
    };
};

controller.delete = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998002 === 1) {
            const { id } = req.params;
            console.log(req.params);
            req.getConnection(function(err, conn) {
                conn.query('DELETE FROM ce611998002 WHERE ID = ?', [id], function(err, bas) {
                    if (err) {
                        const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
                        req.session.errors = errorss;
                        req.session.success = false;
                        res.redirect('/CE611998002');
                    } else {
                        console.log(bas);
                        req.session.success = true;
                        req.session.topic = "ลบสำเร็จ";
                        res.redirect('/CE611998002');
                    }
                });
            });
        } else {
            res.redirect('/ce611998002');
        };
    };
};

controller.edit = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998002 === 1) {
            const { id } = req.params;
            req.getConnection(function(err, conn) {
                conn.query('SELECT * FROM ce611998002  WHERE ID = ?', [id], function(err, bas) {
                    conn.query('SELECT * FROM ce611998019', function(err, tiew) {
                        conn.query('SELECT * FROM ce601998023', function(err, ploy) {
                            if (err) {
                                res.json(err);
                            }
                            res.render('CE611998002/CE611998002Update', {
                                data1: bas[0],
                                data2: tiew,
                                data3: ploy,
                                session: req.session
                            });
                        });
                    });
                });
            });
        } else {
            res.redirect('/ce611998002');
        };
    };
};

controller.update = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998002 === 1) {
            const { id } = req.params;
            const data = req.body;
            if (data.ID_tiew == "") { data.ID_tiew = null; }
            if (data.ID_ploy == "") { data.ID_ploy = null; }
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                req.getConnection(function(err, conn) {
                    conn.query('SELECT * FROM ce611998002 WHERE ID = ?', [id], function(err, bas) {
                        conn.query('SELECT * FROM ce611998019', [id], function(err, tiew) {
                            conn.query('SELECT * FROM ce601998023', [id], function(err, ploy) {
                                if (err) {
                                    res.json(err);
                                }
                                res.render('CE611998002/CE611998002Update', {
                                    data1: bas[0],
                                    data2: tiew,
                                    data3: ploy,
                                    session: req.session
                                });
                            });
                        });
                    });
                });
            } else {
                req.session.success = true;
                req.session.topic = "แก้ไขเรียบร้อย";
                req.getConnection(function(err, conn) {
                    conn.query('update ce611998002 set ? where ID = ?', [data, id], (err, bas) => {
                        if (err) {
                            console.log(err);
                            (err);
                        }
                        res.redirect('/CE611998002')
                    });
                });
            };
        } else {
            res.redirect('/ce611998002');
        };
    };
};


module.exports = controller;