

const controller ={};
const { validationResult } = require('express-validator');


controller.list=(req,res) => {
  if( req.session.user){
  req.getConnection((err,conn) =>{
    conn.query('SELECT s.ID as sID,s.SCIT601998023 as sSCIT601998023, s.MIROT601998023 as sMIROT601998023, s.ID_prem as sID_prem,s.ID_bas as sID_bas,t.SCIT611998005 as tSCIT611998005 , b.SCIT611998002 as bSCIT611998002  FROM CE601998023 as s  left join CE611998005 as t on s.ID_prem = t.id left join CE611998002 as b on s.ID_bas = b.id',(err,CE601998023List) =>{
      if (err) {
        res.json(err);
      }
      res.render('CE601998023/CE601998023List',{ //หาหน้า views ต้องใส่ / ด้วยเพราะมันหนาใน view ไม่เจอ
        data:CE601998023List,
        session: req.session
      });
    });
  });
}else{
    res.redirect('/')
}
};

controller.save=(req,res)=>{
const data=req.body;
if (data.ID_prem=="") {data.ID_prem=null;}
if (data.ID_bas=="") {data.ID_bas=null;}
const errors = validationResult(req);
if( req.session.modulece601998023==1){
    if (!errors.isEmpty()) {
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/CE601998023/add')
    }else {
      req.session.success=true;
      req.session.topic='เพิ่มข้อมูลสำเร็จ';
      req.getConnection((err,conn)=>{
        conn.query('insert into ce601998023 set?',[data],(err,CE601998023List) =>{
          res.redirect('/CE601998023');
 // save ใน customer ไปเลย
  });
});
}
}else{
    res.redirect('/CE601998023')
}
};


controller.del=(req,res)=>{
const { id }=req.params;
if( req.session.modulece601998023==1){
req.getConnection((err,conn)=>{
  conn.query('select * from ce601998023 where id= ?',[id],(err,CE601998023List) =>{
    if (err) {
      const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
      req.session.errors=errorss;
      req.session.success=false;
    }
    res.render('../views/CE601998023/CE601998023Delete',{
      session: req.session,
      data:CE601998023List[0]
    });
  });
});
}else{
    res.redirect('/CE601998023')
}
};

controller.edit=(req,res)=>{
const { id }=req.params;
if( req.session.modulece601998023==1){
req.getConnection((err,conn)=>{
  conn.query('Select * from ce601998023 where id = ?',[id],(err,CE601998023data) =>{
    conn.query('SELECT * FROM ce611998005 ',(err,CE611998005data) =>{
      conn.query('SELECT * FROM ce611998002 ',(err,CE611998002data)=>{res.render
            ('CE601998023/CE601998023Update',{
            data1:CE601998023data[0],
            data2:CE611998005data,
            data3:CE611998002data,
            session: req.session
          });
        });
      });
    });
  });
}else{
    res.redirect('/CE601998023')
}
};

controller.update = (req,res) => {
  const {id} = req.params;
  const data = req.body;

  if (data.ID_prem=="") {data.ID_prem=null;}
  if (data.ID_bas=="") { data.ID_bas=null;}

  const errors = validationResult(req);
  //console.log(data);
      if(!errors.isEmpty()){
        req.session.errors=errors;
        req.session.success=false;
        req.getConnection((err,conn)=>{
          conn.query('Select * from ce601998023 where id = ?',[id],(err,CE601998023data) =>{
            conn.query('SELECT * FROM ce611998005 ',(err,CE611998005data)=>{
              conn.query('SELECT * FROM ce611998002 ',(err,CE611998002data)=>{res.render
                    ('CE601998023/CE601998023Update',{
                    data1:CE601998023data[0],
                    data2:CE611998005data,
                    data3:CE611998002data,
                    session: req.session
                  });
                });
              });
            });
          });

      }else {
        req.session.success=true;
        req.session.topic="แก้ไขเรียบร้อย";
        req.getConnection((err,conn) =>{
          conn.query('update ce601998023 set ? where id = ?',[data,id],(err,CE601998023List)=> {
            if(err){
              res.json(err);
            }
          res.redirect('/CE601998023')
          });
        });
      }

};
controller.delete=(req,res)=>{
const { id }=req.params;
if( req.session.modulece601998023==1){
req.getConnection((err,conn)=>{
  conn.query('Delete from ce601998023 where id= ?',[id],(err,CE601998023List) =>{
    const  errorss= {errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }]}
if (err) {
  req.session.errors=errorss;
  req.session.success=false;
}
    console.log('CE601998023List');
    res.redirect('/CE601998023');
  });
});
}else{
    res.redirect('/CE601998023')
}
};


controller.add =(req,res) => {
  if( req.session.user){
  if( req.session.modulece601998023==1){
  req.getConnection((err,conn) => {
    conn.query("select * from ce611998005",(err,CE611998005data)=> {
      conn.query("select * from ce611998002",(err,CE611998002data)=> {
        res.render('CE601998023/CE601998023Add',{
          data2:CE611998005data,
          data3:CE611998002data,
          session: req.session
          });
        });
      });
    });
  }else{
      res.redirect('/CE601998023')
  }
}else{
    res.redirect('/')
  };
};





module.exports = controller;
