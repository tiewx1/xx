const { check } = require('express-validator');//check คือ ชื่อฟังชั่นที่อยู่ใน express-validator

exports.add = [check('SCIT611998019',"SCIT611998019 -ไม่ถูกต้อง").not().isEmpty(),
               check('MIROT611998019',"MIROT611998019 -ไม่ถูกต้อง").isFloat()
              ];

exports.update = [check('SCIT611998019',"SCIT611998019 -ไม่ถูกต้อง").not().isEmpty(),
                  check('MIROT611998019',"MIROT611998019 -ไม่ถูกต้อง").isFloat()
                 ];
