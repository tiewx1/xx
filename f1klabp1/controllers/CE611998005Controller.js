const controller = {};
const e = require('express');
const { validationResult } = require('express-validator');

controller.list = function(req, res) {
    if (req.session.user) {
        req.getConnection((err, conn) => {
            conn.query('SELECT p.ID as premID , SCIT611998005 ,SCIT611998019,SCIT611998002, MIROT611998005 , p.ID_bas as pID_bas , p.ID_tiew as pID_tiew FROM CE611998005 as p  left join CE611998002 as b on p.ID_bas = b.ID left join CE611998019 as t on p.ID_tiew = t.ID ',
                function(err, prem) {
                    if (err) {
                        res.json(err);
                    } else {
                        res.render('CE611998005/CE611998005List', {
                            data: prem,
                            session: req.session
                        });
                    }
                });
        });
    } else {
        res.redirect('/');
    };
};

controller.add = (req, res) => {
    if (req.session.user) {
        if (req.session.modulece611998005 == 1) {
            req.getConnection((err, conn) => {
                conn.query('SELECT * FROM ce611998002;', function(err, bas) {
                    conn.query('SELECT * FROM ce611998019;', function(err, tiew) {
                        res.render('CE611998005/CE611998005Add', {
                            data2: bas,
                            data3: tiew,
                            session: req.session
                        });
                    });
                });
            });
        } else {
            res.redirect('/CE611998005');
        };
    };
};

controller.save = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998005 === 1) {
            const data = req.body;
            if (data.ID_bas == "") { data.ID_bas = null; }
            if (data.ID_tiew == "") { data.ID_tiew = null; }
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                res.redirect('/CE611998005/add');
            } else {
                req.session.success = true;
                req.session.topic = "เพิ่มข้อมูลสำเร็จ";
                req.getConnection(function(err, conn) {
                    conn.query('insert into ce611998005 set ?', [data], function(err, prem) {
                        res.redirect('/CE611998005');
                    });
                });
            };
        } else {
            res.redirect('/CE611998005');
        };
    };
};

controller.del = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998005 === 1) {
            const { id } = req.params;
            req.getConnection(function(err, conn) {
                conn.query('SELECT p.ID as premID , SCIT611998005 , MIROT611998005 , p.ID_bas as pID_bas , p.ID_tiew as pID_tiew FROM CE611998005 as p  left join CE611998002 as b on p.ID_bas = b.ID left join CE611998019 as t on p.ID_tiew = t.ID HAVING p.ID = ?', [id], function(err, prem) {
                    if (err) {
                        const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลได้', param: '', location: '' }] }
                        req.session.errors = errorss;
                        req.session.success = false;
                    }
                    console.log(prem);
                    res.render('CE611998005/CE611998005Delete', {
                        data: prem[0],
                        session: req.session
                    });
                });
            });
        } else {
            res.redirect('/CE611998005');
        };
    };
};

controller.delete = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998005 === 1) {
            const { id } = req.params;
            console.log(req.params);
            req.getConnection(function(err, conn) {
                conn.query('DELETE FROM ce611998005 WHERE ID = ?', [id], function(err, prem) {
                    if (err) {
                        const errorss = { errors: [{ value: '', msg: 'การลบไม่ถูกต้อง', param: '', location: '' }] }
                        req.session.errors = errorss;
                        req.session.success = false;
                    }
                    console.log(prem);
                    req.session.success = true;
                    req.session.topic = "ลบสำเร็จ";
                    res.redirect('/CE611998005');
                });
            });
        } else {
            res.redirect('/CE611998005');
        };
    };
};

controller.edit = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998005 === 1) {
            const { id } = req.params;
            req.getConnection(function(err, conn) {
                conn.query('SELECT * FROM ce611998005  WHERE ID = ?', [id], function(err, prem) {
                    conn.query('SELECT * FROM ce611998002', [id], function(err, bas) {
                        conn.query('SELECT * FROM ce611998019', [id], function(err, tiew) {
                            if (err) {
                                res.json(err);
                            }
                            res.render('CE611998005/CE611998005Update', {
                                data1: prem[0],
                                data2: bas,
                                data3: tiew,
                                session: req.session
                            });
                        });
                    });
                });
            });
        } else {
            res.redirect('/ce611998005');
        };
    };
};

controller.update = function(req, res) {
    if (req.session.user) {
        if (req.session.modulece611998005 === 1) {
            const { id } = req.params;
            const data = req.body;
            if (data.ID_bas == "") { data.ID_bas = null; }
            if (data.ID_tiew == "") { data.ID_tiew = null; }
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                req.session.errors = errors;
                req.session.success = false;
                req.getConnection(function(err, conn) {
                    conn.query('SELECT * FROM ce611998005  WHERE ID = ?', [id], function(err, prem) {
                        conn.query('SELECT * FROM ce611998002', [id], function(err, bas) {
                            conn.query('SELECT * FROM ce611998019', [id], function(err, tiew) {
                                if (err) {
                                    res.json(err);
                                }
                                res.render('CE611998005/CE611998005Update', {
                                    data1: prem[0],
                                    data2: bas,
                                    data3: tiew,
                                    session: req.session
                                });
                            });
                        });
                    });
                });
            } else {
                req.session.success = true;
                req.session.topic = "แก้ไขเรียบร้อย";
                req.getConnection(function(err, conn) {
                    conn.query('update ce611998005 set ? where ID = ?', [data, id], (err, prem) => {
                        if (err) {
                            console.log(err);
                            (err);
                        }
                        res.redirect('/CE611998005')
                    });
                });
            };
        } else {
            res.redirect('/CE611998005');
        };
    };
};


module.exports = controller;
